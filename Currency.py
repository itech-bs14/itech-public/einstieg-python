#Imports
import requests #Für die Abfrage der API
import json #Verarbeituhng der JSON der API
from currencydict import currencydict #Dictonary für Userfreundlichkeit

#URL für die Abrage
xurl = 'EUR' #URL Endung, wichtig für verschiedene Abfragen
url = f'https://v6.exchangerate-api.com/v6/be5c2195d5f1ef19529b286d/latest/' #Eigentliche URL

#Funktion um die Daten abzurufen und anschließend zu speichern also die JSON datei erstellen
#Abrufen der Daten
def daten_abrufen(xurl):
    finalurl = url + xurl #Erstellung der gewünschten URL

    response = requests.get(finalurl) #API Abfrage
    data = response.json() #Das Json File in Python lade
    print("Daten werden abgerufen")
    #Speichern der Daten
    with open ('currency.json', 'w') as json_file: #W für Write, hier wird gespeichert
        json.dump(data, json_file, ensure_ascii=False, indent=4)

    with open ('currency.json', 'r') as f: #Hier werden die Daten direkt wieder gelesen um sie ggf.
        data =json.load(f)                  # weiter zu verarbeiten
        print("Daten wurden gespeichert")

#Funktion für das Main menu
def main_menu():
    while True:
        print_menu()
        choice = input("Auswahl treffen\n")

        if choice == '1' : #Print Befehl aller Währungen
            option1()
        elif choice == '2': #Abkürzungssuche
            option2()
        elif choice == '3':#Umrechnungsfunktion
            option3()
        elif choice == '4':#Daten aktualisieren
            daten_abrufen()
        elif choice == '5': #Hilfestellung
            print("Für Hilfestellung bitte in die ReadMe schauen")
        elif choice == '6': #Exit
            exitProgramm()
        else:
            print("Auswahl nicht möglich")

#Funktion print main menu
def print_menu():
    print("Willkommen im Hauptmenü")
    print("1. Komplettes Dictonary anzeigen lassen")
    print("2. Nach einer bestimmten Währung suchen")
    print("3. Umrechnen")
    print("4. Daten aktualisieren")
    print("5. Hilfestellung")
    print("6. Programm verlassen")

#Funktion für Print der Gesamten Currencies
def option1():
    print("Die Währungen mit ihren Abkürzen werden angezeigt")
    for key, value in currencydict.items():     #Print mit Zeilenumbruch
        print(f"{key}: {value}")

#Funktion Abkürzungssuche
def option2():
    print("Möchten Sie...\n")
    print("Mit Hilfe einer Abkürzung eine Währung finden?")                     #Abkürzung -> Währung
    print("Mit Hilfe einer Währung eine Abkürzung finden?")                     #Währung -> Abkürzung
    print("Zurück ins Hauptmenü\n")
    while True: #Schleife für Switch Case
        choice = input("Bitte Auswahl 1, 2 oder 3 treffen\n")

        if choice == '1' :                                                      #Abkürzung -> Währung
            print("Zu welcher Abkürzung möchten Sie die Währung?\n")
            print("Währungen werden immer mit drei Buchsstaben abgekürzt!\n")
            currency = input()
            print(currencydict[currency])                                       #Zugriff auf unser Dict
        elif choice == '2':                                                     #Währung -> Abkürzung
            print("Zu welcher Währung wollen Sie die Abkürzung?\n")
            print("Die Währung bitte ohne Rechtschreibfehler angeben\n")
            currency = input()
            print(currencydict[currency])                                       #Zugriff auf unser Dict
        elif choice == '3':
            print("Zurück ins Hauptemenü")
            main_menu()
            break
        else:
            print("Auswahl nicht möglich\n")

#Funktion Umrechnung
def option3():
    print("Option 3 gewählt")
    xurl = input("Welche Währung soll getauscht werden?\n") #Abfrage welches Geld der Anwender hat
    daten_abrufen(xurl)                                                         #Currency.Json auf gewünschte Währung abrufen
    targetcurrency = input("In welche Währung soll getauscht werden?\n")#Abfrage für Zielwährung
    with open('currency.json', 'r') as f: #Json File in Python laden
        data = json.load(f)

    rate = data['conversion_rates'][targetcurrency] #Zugriff auf den Kurs
    print("Der aktuelle Wechselkurst beträgt")
    print(rate)
    amount = float(input("Wie viel Geld soll getauscht werden?\n"))
    final = (rate * amount) #Rechnung Zielbetrag
    print(final)
    print("Zurück zum Hauptemnü")
    main_menu()

#Funktion Programm schließen
def exitProgramm():
    print("Programm wird geschlossen\n")
    exit()




main_menu()








