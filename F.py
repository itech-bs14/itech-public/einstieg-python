import requests

API_KEY = "41993f8ae0613d78a932b5eca483ab44"
city = input("Hallo! Bitte gib eine Stadt ein.")

url = f"https://api.openweathermap.org/data/2.5/weather?q={city}&appid={API_KEY}&units=metric"

data = requests.get(url).json()

temparatur = data["main"]["temp"]
feuchtigkeit = data["main"]["humidity"]

print(f"in {city} beträgt die Temperatur {temparatur}.")
print(f"in {city} beträgt die Luftfeuchigkeit {feuchtigkeit}.")
