# Pretty printing JSON Returned from an API
import requests
import json

response = requests.get("https://www.boredapi.com/api/activity")
json_response = response.json()
pretty_response = json.dumps(json_response, indent=4)

# You could also write:
pretty_response = json.dumps(response.json(), indent=4)

print(pretty_response)

# Returns:
# {
#     "activity": "Have a football scrimmage with some friends",
#     "type": "social",
#     "participants": 8,
#     "price": 0,
#     "link": "",
#     "key": "1638604",
#     "accessibility": 0.2
# }