import imdb

# Initialisierung der Cinemagoer-API
ia = imdb.Cinemagoer()

# Vordefinierte Keywords für verschiedene Stimmungen
mood_keywords = {
    "glücklich": ["high school", "fairy tale", "satire"],
    "traurig": ["death", "terminal illness", "loss"],
    "ängstlich": ["psychological thriller", "suspense", "horror"],
    "gelassen": ["romantic comedy", "feel-good", "adventure"],
    "aufgeregt": ["action-packed", "thrilling", "suspenseful"],
    "gestresst": ["crime", "heist", "mystery"],
    "verärgert": ["revenge", "betrayal", "vigilante"],
    "verwirrt": ["mind-bending", "psychological", "twists and turns"],
    "entspannt": ["feel-good", "romance", "comedy"],
    "eifersüchtig": ["romantic rivalry", "betrayal", "love triangle"],
    "einsam": ["solitude", "lost love", "isolation"],
    "dankbar": ["inspirational", "uplifting", "feel-good"],
    "hoffnungsvoll": ["inspiring", "optimistic", "uplifting"],
    "zufrieden": ["contentment", "warm-hearted", "nostalgic"],
    "enttäuscht": ["disappointment", "loss", "deception"],
    "belastet": ["tension", "psychological", "emotional turmoil"],
    "nervös": ["edge-of-your-seat", "intense", "suspenseful"],
    "erleichtert": ["light-hearted", "comedy", "relief"],
    "beschämt": ["embarrassment", "awkward situations", "comedy"],
    "empathisch": ["touching", "emotional", "relatable"]
}


def search_movies_by_keywords(keywords):
    # Abrufen von Filmen basierend auf den angegebenen Keywords
    found_movies = []
    for keyword in keywords:
        movies = ia.get_keyword(keyword)
        found_movies.extend(movies)
    return found_movies[:5]  # Begrenzen auf die ersten 5 Filme


def get_user_mood():
    # Benutzereingabe für die Stimmung
    user_mood = input("Bitte gib deine Stimmung ein (glücklich, traurig, ängstlich, usw.): ").lower()
    return user_mood


def main():
    # Stimmung des Benutzers abfragen
    user_mood = get_user_mood()

    # Keyword basierend auf der Stimmung des Benutzers abrufen
    if user_mood in mood_keywords:
        keywords = mood_keywords[user_mood]

        # Filme basierend auf den Keywords suchen
        found_movies = search_movies_by_keywords(keywords)

        # Ausgabe der gefundenen Filme
        print("Filme, die zur Stimmung '{}' passen:".format(user_mood.capitalize()))
        for movie in found_movies:
            print("- ", movie['title'])
    else:
        print("Die Stimmung '{}' ist nicht verfügbar.".format(user_mood))


if __name__ == "__main__":
    main()
