import requests
import smtplib
import sys  # Füge diese Zeile hinzu, um das sys-Modul zu importieren

API_Key = "3a92e8764c861d234f3fac55f344a7fa"

lat = "53.3574"
lon = "10.2127"

requests_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={API_Key}"
response = requests.get(requests_url)

if response.status_code == 200:
    data = response.json()
    print(data)
    weather_description = data["weather"][0]["description"]
    print(weather_description)
    temperature = round(data["main"]["temp"] - 273.15)
    print(temperature, "celsius")
    temp_max = round(data["main"]["temp_max"] - 273.15)
    print(temp_max, "Max_celsius_today")
else:
    sys.exit("Fehler")  # Verwende sys.exit anstelle von return außerhalb einer Funktion
