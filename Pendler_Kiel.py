import matplotlib.pyplot as plt
import csv

filename: str = "Pendler.csv"

with open(filename) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=",")

    header_row = next(csv_reader)

    einpendler: list[int] = []
    auspendler: list[int] = []
    jahr: list[str] = []

    for row in csv_reader:
        einpendler.append(int(row[1]))
        auspendler.append(int(row[2]))
        jahr.append(row[0])

plt.style.use("bmh")

fig = plt.figure(dpi=144, figsize=(10, 6))

plt.plot(jahr, einpendler, c="blue")
plt.plot(jahr, auspendler, c="red")

plt.title("Berufspendler in Kiel\n1994-2022", size=18)

plt.xlabel("Jahr", size=14)
fig.autofmt_xdate()
plt.ylabel("Anzahl der Berufspendler", size=14)

plt.show()