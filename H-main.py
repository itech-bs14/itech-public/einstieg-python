import requests

while True:

    city = input('Geben Sie eine Stadt ein: ')

    url = f'http://api.openweathermap.org/data/2.5/weather?q={city}&appid=7ee0d8ec71f96a132b87c9f2eb3b890b&units=metric&lang=de'

    res = requests.get(url)
    data = res.json()

    if data["cod"] != "404":

        temp = data['main']['temp']
        wind_speed = data['wind']['speed']
        description = data['weather'][0]['description']

        print(f'Temperatur: {temp} °C')
        print(f'Windgeschwindigkeit: {wind_speed} m/s')
        print(f'Wetterverhältnis: {description}')

        break

    else:
        print("Stadt nicht gefunden. Bitte versuchen Sie es erneut.")
