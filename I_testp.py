import openmeteo_requests
import requests_cache
from retry_requests import retry
import datetime

# Setup the Open-Meteo API client with cache and retry on error
cache_session = requests_cache.CachedSession('.cache', expire_after = 3600)
retry_session = retry(cache_session, retries = 5, backoff_factor = 0.2)
openmeteo = openmeteo_requests.Client(session = retry_session)

# Make sure all required weather variables are listed here
# The order of variables in hourly or daily is important to assign them correctly below
url = "https://api.open-meteo.com/v1/forecast"
params = {
	"latitude": 53.56,
	"longitude": 9.9103,
	"current": ["temperature_2m", "rain", "showers", "snowfall", "cloud_cover", "wind_speed_10m", "wind_direction_10m"],
	"timezone": "Europe/Berlin",
	"forecast_days": 1
}
responses = openmeteo.weather_api(url, params=params)

# Process first location. Add a for-loop for multiple locations or weather models
response = responses[0]
print(f"Coordinates {response.Latitude()}°N {response.Longitude()}°E")
print(f"Timezone {response.Timezone()} {response.TimezoneAbbreviation()}")


# Current values. The order of variables needs to be the same as requested.
current = response.Current()
current_temperature_2m = float(current.Variables(0).Value())
current_rain = float(current.Variables(1).Value())
current_showers = float(current.Variables(2).Value())
current_snowfall = float(current.Variables(3).Value())
current_cloud_cover = float(current.Variables(4).Value())
current_wind_speed_10m = float(current.Variables(5).Value())
current_wind_direction_10m = float(current.Variables(6).Value())


ts = int(current.Time())

print(datetime.datetime.fromtimestamp(current.Time(), datetime.UTC))
#print(f"Current time {current.Time()}")
print(f"Current temperature_2m {round(current_temperature_2m,2)}")
print(f"Current rain {round(current_rain,2)}")
print(f"Current showers {round(current_showers,2)}")
print(f"Current snowfall {round(current_snowfall,2)}")
print(f"Current cloud_cover {round(current_cloud_cover,2)}")
print(f"Current wind_speed_10m {round(current_wind_speed_10m,2)}")
print(f"Current wind_direction_10m {round(current_wind_direction_10m,2)}")
