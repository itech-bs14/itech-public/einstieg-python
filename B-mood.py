import imdb
import random
import json

# Initialisierung der Cinemagoer-API
ia = imdb.Cinemagoer()

# Vordefinierte Keywords für verschiedene Stimmungen
positive_moods = {
    "glücklich": ["high-school", "fairytale", "satire"],
    "gelassen": ["romantic-comedy", "feel-good", "adv enture"],
    "aufgeregt": ["action-packed", "thrilling", "suspenseful"],
    "entspannt": ["feel-good", "romance", "comedy"],
    "dankbar": ["inspirational", "uplifting", "feel-good"],
    "hoffnungsvoll": ["inspiring", "optimistic", "uplifting"],
    "zufrieden": ["contentment", "warm-hearted", "nostalgic"],
    "erleichtert": ["light-hearted", "comedy", "relief"]
}

negative_moods = {
    "traurig": ["death", "terminal-illness", "loss"],
    "ängstlich": ["psychological-thriller", "suspense", "horror"],
    "gestresst": ["crime", "heist", "mystery"],
    "verärgert": ["revenge", "betrayal", "vigilante"],
    "verwirrt": ["mind-bending", "psychological", "twists-and-turns"],
    "eifersüchtig": ["romantic-rivalry", "betrayal", "love-triangle"],
    "einsam": ["solitude", "lost-love", "isolation"],
    "enttäuscht": ["disappointment", "loss", "deception"],
    "belastet": ["tension", "psychological", "emotional-turmoil"],
    "nervös": ["edge-of-your-seat", "intense", "suspenseful"],
    "beschämt": ["embarrassment", "awkward-situations", "comedy"]
}


def search_movies_by_keywords(keywords):
    # Abrufen von Filmen basierend auf den angegebenen Keywords
    found_movies = []
    for keyword in keywords:
        movies = ia.get_keyword(keyword)
        found_movies.extend(movies)

    # Wähle zufällige 5 Filme aus
    final_movies = random.sample(found_movies, min(5, len(found_movies)))
    return final_movies


def display_help():
    print("Movie Mood Finder Hilfe:")
    print("- Verwende dieses Programm, um Filme basierend auf deiner aktuellen Stimmung zu finden.")
    print("- Wähle entweder '1' -> positiv oder '2' -> negativ für deine Stimmung aus.")
    print("- Anschließend kannst du eine spezifische Stimmung aus der angezeigten Liste auswählen.")
    print("- Das Programm gibt dann zufällig ausgewählte Filme basierend auf deiner Stimmung aus.")


def display_welcome_menu():
    print("Willkommen zum Movie Mood Finder!")
    print("Wie fühlst du dich?:")
    print("1. Eher angenehm :)")
    print("2. Eher unwohl :(")


def display_mood_menu(mood_dict):
    print("\nBitte wähle eine Stimmung aus:")
    for index, mood in enumerate(mood_dict.keys(), start=1):
        print(f"{index}. {mood.capitalize()}")


def get_user_choice():
    while True:
        choice = input("Deine Auswahl: ")
        if choice == "-h":
            display_help()
        elif choice.isdigit():
            return choice
        else:
            print("Ungültige Eingabe. Bitte gib eine Nummer ein oder verwende '-h' für die Hilfe.")
            continue  # Fortsetzen der Schleife, um erneute Eingabe zu ermöglichen


# Schreibt die Daten in die JSON-Datei mit Einrückung für bessere Lesbarkeit
def save_data_to_json(data, filename):
    with open(filename, 'w') as json_file:
        json.dump(data, json_file, indent=4)


# Konvertiert die Filmdaten aus der API in ein serialisierbares Format für die JSON-Ausgabe.
def convert_movies_to_json_serializable(final_movies):
    serializable_movies = []
    for movie in final_movies:
        serializable_movie = {
            "title": movie.get('title'),
            "year": movie.get('year', 'unbekannt')
        }
        # Hinzufügen des neuen Element serializable_movie zu der Liste  seriazable_movies hinzu
        serializable_movies.append(serializable_movie)
    return serializable_movies

# Die Hauptfunktion des Programms, die den Ablauf steuert.
def main():
    # Willkommensmenü anzeigen
    display_welcome_menu()

    # Auswahl zwischen positiven und negativen Stimmungen treffen
    choice = False
    while not choice:
        mood_category = get_user_choice()
        if mood_category == "1":
            mood_dict = positive_moods
            choice = True
        elif mood_category == "2":
            mood_dict = negative_moods
            choice = True
        elif mood_category == "-h":
            display_help()
        else:
            print("Ungültige Auswahl. Bitte wähle 1 oder 2.")

    # Stimmungsmenü anzeigen mit der ausgewählten Stimmung des Nutzers.
    display_mood_menu(mood_dict)

    # Stimmung des Benutzers abfragen
    mood_choice = get_user_choice()
    moods = list(mood_dict.keys())
    user_mood = moods[int(mood_choice) - 1]

    # Keywords basierend auf der Stimmung des Benutzers abrufen
    keywords = mood_dict[user_mood]

    # Filme basierend auf den Keywords suchen
    final_movies = search_movies_by_keywords(keywords)

    # Ausgabe der gefundenen Filme
    print("\nFilme, die zur Stimmung '{}' passen:".format(user_mood.capitalize()))
    for movie in final_movies:
        title = movie.get('title')
        year = movie.get('year', 'unbekannt')
        print("- ", title, year)

    # Versuch die ausgegebenen API-Daten in der JSON-Datei abzuspeichern
    if final_movies:
        serializable_movies = convert_movies_to_json_serializable(final_movies)
        save_data_to_json(serializable_movies, 'api_daten.json')
        print("Daten wurden erfolgreich in api_daten.json gespeichert")
    else:
        print("Keine Daten zum Speichern vorhanden")


if __name__ == "__main__":
    main()
