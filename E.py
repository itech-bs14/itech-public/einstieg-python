import requests  # Importieren der requests-Bibliothek, um HTTP-Anfragen zu senden

API_KEY = "f45516baebd64c37c489dfa87a07a11f"  # API-Schlüssel für die OpenWeatherMap-API

lat = "53.5502"  # Breitengrad der gewünschten Position
lon = "9.9920"   # Längengrad der gewünschten Position

# Die URL für die Wetter-API erstellen, indem Breiten- und Längengrade sowie der API-Schlüssel eingefügt werden
request_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={API_KEY}"

# Eine HTTP-GET-Anfrage an die OpenWeatherMap-API senden, um Wetterdaten abzurufen
response = requests.get(request_url)

# Überprüfen, ob die Anfrage erfolgreich war (Statuscode 200)
if response.status_code == 200:
    data = response.json()  # Die Antwort in das JSON-Format umwandeln
    print(data)  # Die Wetterdaten ausgeben
else:
    print("Fehler")  # Eine Fehlermeldung ausgeben, falls die Anfrage nicht erfolgreich war
