#   044d9edd65a0494794ebbc2d0a07a8fb = Client ID
#   391e225dfd4a4212a4bac46c8f3f29db = Client Secret

import os
import json
import myspotipy
from myspotipy.oauth2 import SpotifyClientCredentials

# sämtliche Textausgaben der Commandline festgelegt als strings
error_txt = "Ungültige Eingabe. Versuche erneut. \n"

cmd_welcome = ("\n\nHerzlich Wilkommen bei der LueKa-API.               [Developer: Lennart Kahl, Onno Lübchow] \n\n"
               "Die LueKa-API dient dir dazu,"
               " komplett automatisch eine eigene Spotify Playlist je nach Stimmungslage zu erstellen. \n"
               "Hierfür benötigen wir nur ein paar wenige Angaben von dir. \n"
               "Du hast die Möglichkeit deine Angaben über eine Json-Datei und über die Konsole per Hand einzugeben. \n"
               "Außerdem werden deine angegebenen Daten automatisch in einer Json-Dabei speichern. \n"
               "Dafür haben wir bereits einen Ordner Namens LueKa_API auf deinem Desktop erstellt. \n"
               "Wie möchtest du fortfahren? Konsole(k) / Json-Datei(j): \n")
cmd_info_01 = "Benutzereingabe per Konsole ausgewählt. Bitte beachte folgendes: \n"
cmd_info_02 = '- mit "*" markierte Fragen müssen ausgefüllt werden \n'
cmd_info_03 = "- nicht markierte Fragen sind optional. Sie haben einen Standardwert der bei keiner Angabe übernommen wird \n"
cmd_info_04 = "- wenn du fertig bist drücke die (0) um deine Angaben zu bestätigen und fortzufahren \n"
cmd_help = f"{cmd_info_02}{cmd_info_03}{cmd_info_04}Bei weiteren Fragen wende dich bitte an unsere Developer \n"
cmd_choice = "Welche Kategorie möchtest du aufrufen? (1-6)\n"
cmd_username = "Wie lautet dein Spotify Nutzername? \n"
cmd_pl_name = "Wie soll deine neue Playlist heißen? \n"
cmd_description = "Wie soll die Beschreibung deiner Playlist lauten? \n"
cmd_public = "Soll deine Playlist öffentlich zugänglich sein? (Ja/Nein) \n"
cmd_collaborative = "Soll diese Playlist eine geteilte Playlist werden? (Ja/Nein) \n"
cmd_confirmation = "Sicher das du fertig bist? Deine Angaben kannst du danach nicht mehr ändern (Ja/Nein) \n"
cmd_end = "Vielen Dank. Ihre Playlist wurde erfolgreich erstellt und das Programm wird beendet."
from_json_info = "JSON INFO \n"
json_file_input = "Bitte gebe hier den Laufwerkspfad der Json Datei ein: \n"


# Konstruktor Playlist
class Playlist:
    def __init__(self, username = None, playlist_name = None, description = "LueKa", public = False, collaborative = False):
        # Benötigten Bedingungen/ Bausteine
        self.username = username
        self.playlist_name = playlist_name

        # optionale Bedingungen/Bausteine
        self.description = description
        self.public = public
        self.collaborative = collaborative

    # Abfrage der Bedingungen per Konsole
    def from_cmd(self):
        conditions_list = ["Nutzername *", "Playlistname *", "Beschreibung (LueKa)", "Öffentlichkeit (Nein)", "Gemeinschaftsplaylist (Nein)"]
        for i, attr in enumerate(conditions_list, start = 1):
            print(f"{i}. {attr}")                                             # Ausgabe der Menüpunkte
        print("-h. Hilfe")
        print()

        while True:
            cmd_user_choice = input(f"{cmd_choice}")                          # Abfrage Kategorieauswahl
            if cmd_user_choice == "1":
                print(conditions_list[0])
                self.username = input(f"{cmd_username}")
                print(f"{conditions_list[0]}: {self.username} \n")
            elif cmd_user_choice == "2":
                print(conditions_list[1])
                self.playlist_name = input(f"{cmd_pl_name}")
                print(f"{conditions_list[1]}: {self.playlist_name} \n")
            elif cmd_user_choice == "3":
                print(conditions_list[2])
                self.description = input(f"{cmd_description}")
                print(f"{conditions_list[2]}: {self.description} \n")
            elif cmd_user_choice == "4":
                print(conditions_list[3])
                self.public = input(f"{cmd_public}")
                while self.public not in ["ja", "nein"]:                     # Loopback ungültige Eingabe/ bool expected
                    print(f"{error_txt}")
                    self.public = input(f"{cmd_public}")
                if self.public.lower() == "ja":                              # ja = True
                    self.public = True
                else:                                                        # nein = False
                    self.public = False
                print(f"Öffentlichkeit: {self.public} \n")
            elif cmd_user_choice == "5":
                print(conditions_list[4])
                self.collaborative = input(f"{cmd_collaborative}")
                while self.collaborative not in ["ja", "nein"]:
                    print(f"{error_txt}")
                    self.collaborative = input(f"{cmd_collaborative}")
                if self.collaborative.lower() == "ja":
                    self.collaborative = True
                else:
                    self.collaborative = False
                print(f"Gemeinschaftsplaylist: {self.collaborative} \n")
            elif cmd_user_choice == "-h":                                    # Hilfestellung Anforderung
                print("Hilfe")
                print(f"{cmd_help}")
            elif cmd_user_choice == "0":                                     # Unterbrechung der Schleife
                return
            else:
                print(f"{error_txt}")

    # aus einer .json Datei lesen
    @classmethod
    def from_json(cls, json_file):
        with open(json_file, 'r') as file:
            json_data = json.load(file)
        return cls(json_data["username"], json_data["playlist_name"], json_data["description"],
                   public=json_data.get("public", False),
                   collaborative=json_data.get("collaborative", False))

    # Playlist Objekt in Dictionary umwandeln für Json Übergabe
    def json_dict(self):
        return{
    "Username": self.username,
    "Playlist Name": self.playlist_name,
    "Beschreibung": self.description,
    "Veröffentlichung": self.public,
    "Geteilte Playlist" : self.collaborative
    }


# Ordner für Json Datei wird erstellt
def create_json_dict():
    # dict_path = "C:\\Users\\lennart\\Desktop"
    dict_path = "C:\\Users\\lennart.kahl\\OneDrive - NetcomCS\\Desktop"
    dict_name = "LueKa_API"

    target_dict_path = os.path.join(dict_path, dict_name)
    if not os.path.exists(target_dict_path):                # Überprüfen ob der Ordner existiert
        os.makedirs(target_dict_path)


# Eigentlicher Programmablauf definiert als Funktion Main()
def main():
    create_json_dict()
    new_playlist = Playlist()
    user_choice = input(f"{cmd_welcome}")
    while user_choice.lower() not in ["j", "k"]:                          # Loopback bei Falscher Eingabe
        print(f"{error_txt}")
        user_choice = input(f"{cmd_welcome}")

    if user_choice.lower() == "j":                                        # Datenerfassung via Json Datei
        print(f"{from_json_info}")
        json_file = input(f"{json_file_input}")
        new_playlist.from_json(json_file)

    if user_choice.lower() == "k":                                        # Datenerfassung via Konsole
        print(f"{cmd_info_01}{cmd_info_02}{cmd_info_03}{cmd_info_04}", end ="\n")
        while True:
            new_playlist.from_cmd()
            user_choice = input(f"{cmd_confirmation}")
            if user_choice.lower() == "nein":
                continue
            else:
                break
        playlist_data = new_playlist.json_dict()
        dict_path = "C:\\Users\\lennart\\Desktop\\LueKa_API"
        json_file_path = os.path.join(dict_path, "Playlist-Data.json")
        if not os.path.exists(json_file_path):
            with open(json_file_path, "w") as json_file:
                json.dump(playlist_data, json_file)
        print(f"{cmd_end}")


# Programmausführung
if __name__ == "__main__":
    main()
