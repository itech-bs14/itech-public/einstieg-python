import requests

def get_5_day_forecast(city, api_key):
	url = f"http://api.openweathermap.org/data/2.5/forecast?q={city}&appid={api_key}&units=metric&lang=de"
	response = requests.get(url)
	if response.status_code == 200:  # Prüft, ob die Anfrage erfolgreich war
		forecast_data = response.json()
		for forecast in forecast_data['list']:
			time = forecast['dt_txt']
			temp = forecast['main']['temp']
			description = forecast['weather'][0]['description']
			print(f"Zeit: {time}, Temperatur: {temp}°C, Beschreibung: {description}")
			break
	else:
		print("Fehler bei der Abfrage der Wettervorhersage.")

# Beispielverwendung
api_key = "7ee0d8ec71f96a132b87c9f2eb3b890b"
city = "Hamburg"
get_5_day_forecast(city, api_key)
